package br.com.n4w3b.educafacilapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducaFacilApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EducaFacilApiApplication.class, args);
	}

}
