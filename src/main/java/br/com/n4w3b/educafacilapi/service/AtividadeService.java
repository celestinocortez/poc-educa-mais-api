package br.com.n4w3b.educafacilapi.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

import br.com.n4w3b.educafacilapi.dto.AtividadeDTO;
import br.com.n4w3b.educafacilapi.model.Atividade;
import br.com.n4w3b.educafacilapi.repository.AtividadeRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AtividadeService {

	private final AtividadeRepository repository;

	public Atividade uploadComDados(AtividadeDTO atividadeDTO) throws IOException {

		Atividade atividade = new Atividade();

		atividade.setNome(atividadeDTO.getNome());
		atividade.setTag(atividadeDTO.getTag());

		if (atividadeDTO.getArquivo() != null) {
			atividade.setArquivo(atividadeDTO.getArquivo().getBytes());
			atividade.setArquivoNome(atividadeDTO.getArquivo().getOriginalFilename());
			atividade.setArquivoExtensao(atividadeDTO.getArquivo().getContentType());
			atividade.setArquivoTamanho(atividadeDTO.getArquivo().getSize());
		}

		return repository.save(atividade);
	}

}
