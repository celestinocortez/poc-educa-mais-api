package br.com.n4w3b.educafacilapi.controller;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.n4w3b.educafacilapi.dto.AtividadeDTO;
import br.com.n4w3b.educafacilapi.model.Atividade;
import br.com.n4w3b.educafacilapi.service.AtividadeService;
import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/atividade")
@RequiredArgsConstructor
public class AtividadeController {

	private final AtividadeService service;

	@PostMapping("/upload-com-dados")
	public ResponseEntity<Atividade> uploadComDados(@ModelAttribute AtividadeDTO atividadeDTO) throws IOException {
		return ResponseEntity.ok(service.uploadComDados(atividadeDTO));
	}

}
